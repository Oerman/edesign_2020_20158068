/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "my_user.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac){

	HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);
	wave_fillbuffer(Dacbuffer+512, Sine, 512);
}

void HAL_DAC_ConvHalfCpltCallbackCh1(DAC_HandleTypeDef* hdac){

	wave_fillbuffer(Dacbuffer, Sine, 512);
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_DAC_Init();
  MX_TIM3_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
wave_init();
HAL_TIM_Base_Start(&htim3);
HAL_UART_Transmit(&huart2, buffer_tx, 10, 100);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  //Button 1 pressed playback
	  	  if(button1_pressed==1){
	  		  //debouce the button for 10ms
	  		  if((HAL_GetTick()-pretick) > 10){

	  			 if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)==1){
	  			  //change pin state

	  				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,0);
	  				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,0);
	  				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,1);
	  				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);

	  				Sine = 1;
	  				PlaybackData(Sine);

	  			  if((flashflag)==1){
	  				  flashflag=0;
	  				  flashtick=0;
	  				HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6);
	  			  }
	  			HAL_UART_Transmit(&huart2, buffer_play1, 10, 100);

	  			 }

	  			pretick = HAL_GetTick();
	  		  }
	  	  }else{
	  		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
	  		  button1_pressed=0;


	  	  }

	  		  //Button 2 pressed
	  	 if(button2_pressed==1){
	  		  	//debouce the button for 10ms
	    		  if((HAL_GetTick()-pretick) > 10){
	    			  //change pin state
	    			  if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)==1){

	    			  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,0);
	    			  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,0);
	    			  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
	    			  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,1);

	    			  Sine = 2;
	    			  PlaybackData(Sine);

	    			  if((flashflag)==1){
	    				  flashflag=0;
	    				  flashtick=0;
	    				  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7);
	    			  }
	    			  HAL_UART_Transmit(&huart2, buffer_play2, 10, 100);
	    			  Sine = 2;

	    		  }

	    		  pretick = HAL_GetTick();
	    		  }
	    		  }else{
	  		  		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
	  		  		  button2_pressed=0;

	  		  	  }
	    		  //Button 3 pressed
	    	if(button3_pressed==1){
	    		//debouce the button for 10ms
	    		if((HAL_GetTick()-pretick) > 10){

	    			//change pin state
	    			if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)==1){


	    				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,1);
	    				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,0);
	    				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
	    				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);

	    				Sine = 3;
	    				PlaybackData(Sine);

	    				if((flashflag)==1){
	    					flashflag=0;
	    					flashtick=0;
	    					HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);
	    				}
	    				HAL_UART_Transmit(&huart2, buffer_play3, 10, 100);

	    			}
	    			pretick = HAL_GetTick();
	    		}
	    		}else{
	    				  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0);
	    				  button3_pressed=0;

	  	  }
	  //stop button pressed
	    	if(buttonSTOP_pressed==1){
	    		//debouce the button for 10ms
	    		if((HAL_GetTick()-pretick) > 10){

	    			if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)==1){

	    	button1_pressed=0;
	  		button2_pressed=0;
	  		button3_pressed=0;
	  		buttonREC_pressed=0;
	  		buttonSTOP_pressed=0;
	  		Sine=0;
	  		ButtonWithREC=0;
	  		memset(Dacbuffer, 0, sizeof Dacbuffer);

	  		 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0);
	  		 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
	  		 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, 0);
	  		 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);


	  	 HAL_UART_Transmit(&huart2, buffer_STOP, 10, 100);
	    			}
	    		}

	    	}

	    	//Record button pressed
	    	if(buttonREC_pressed==1){
	    		//debouce the button for 10ms
	    		if((HAL_GetTick()-pretick) > 10){

	    			while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_11)==1){

	    				if(button1_pressed==1){

	    				//debouce the button for 10ms
	    					if((HAL_GetTick()-pretick) > 10){
	    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)==1){
	    							//Byte value for ASCII 1
	    							ButtonWithREC=49;

	    							if((flashflag)==1){
	    								flashflag=0;
	    								flashtick=0;
	    								HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);

	    						}
	    							pretick = HAL_GetTick();
	    					}
	    				}
	    		}

	    				if(button2_pressed==1){

	    					//debouce the button for 10ms
	    					if((HAL_GetTick()-pretick) > 10){
	    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)==1){
	    							//Byte value for ASCII 2
	    							ButtonWithREC=50;

	    							if((flashflag)==1){
	    								flashflag=0;
	    								flashtick=0;
	    								HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7);

	    				}
	    							pretick = HAL_GetTick();
	    			}
	    		  }
	    		}

	    				if(button3_pressed==1){
	    					//debouce the button for 10ms
	    					if((HAL_GetTick()-pretick) > 10){
	    						if((HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15))==1){
	    							//Byte value for ASCII 3
	    							ButtonWithREC=51;

	    							if((flashflag)==1){
	    								flashflag=0;
	    								flashtick=0;
	    								HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_4);

	    			}
	    							pretick = HAL_GetTick();
	    			}
	    		  }
	    		}




	    		}
	    	}

	    		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);
	    		uint8_t buffer_REC[10] = {127,128,'R','E','C','O','R','D','_',ButtonWithREC};
	    		HAL_UART_Transmit(&huart2, buffer_REC, 10, 100);

	    	}


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief DAC Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC_Init(void)
{

  /* USER CODE BEGIN DAC_Init 0 */

  /* USER CODE END DAC_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC_Init 1 */

  /* USER CODE END DAC_Init 1 */
  /** DAC Initialization 
  */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT1 config 
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_T2_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC_Init 2 */

  /* USER CODE END DAC_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1905;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 500000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|GPIO_PIN_6|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4|GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pins : LD2_Pin PA6 PA7 */
  GPIO_InitStruct.Pin = LD2_Pin|GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB12 PB13 PB14 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PA11 */
  GPIO_InitStruct.Pin = GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB4 PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
