/*
 * my_globals.c
 *
 */
#include <stdio.h>
#include <stdbool.h>

uint8_t buffer_tx[10] = {127,128,'2','0','1','5','8','0','6','8'};
uint8_t buffer_play1[10] = {127,128,'P','L','A','Y','_','_','_','1'};
uint8_t buffer_play2[10] = {127,128,'P','L','A','Y','_','_','_','2'};
uint8_t buffer_play3[10] = {127,128,'P','L','A','Y','_','_','_','3'};
uint8_t buffer_STOP[10] = {127,128,'S','T','O','P','_','_','_','_'};
uint16_t Dacbuffer[1024];


int pretick;
int flashtick;
uint8_t ButtonWithREC;

volatile int flashflag;
volatile uint8_t button1_pressed =0;
volatile uint8_t button2_pressed =0;
volatile uint8_t button3_pressed =0;
volatile uint8_t buttonREC_pressed =0;
volatile uint8_t buttonSTOP_pressed =0;
volatile uint8_t Sine =0;


