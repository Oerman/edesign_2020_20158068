/*
 * my_user.h
 *
 *  Created on: 11 Mar 2020
 *      Author: Dewal
 */

#ifndef MY_USER_H_
#define MY_USER_H_

#include "main.h"
#include "stm32f4xx_it.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

extern UART_HandleTypeDef huart2;
extern DAC_HandleTypeDef hdac;
extern DMA_HandleTypeDef hdma_dac1;

extern int pretick;
extern int flashtick;
extern volatile int flashflag;
extern uint8_t ButtonWithREC;

extern volatile uint8_t button1_pressed;
extern volatile uint8_t button2_pressed;
extern volatile uint8_t button3_pressed;
extern volatile uint8_t buttonREC_pressed;
extern volatile uint8_t buttonSTOP_pressed;
extern volatile uint8_t Sine;

extern uint8_t buffer_tx[10];
extern uint8_t buffer_play1[10];
extern uint8_t buffer_play2[10];
extern uint8_t buffer_play3[10];
extern uint8_t buffer_STOP[10];
extern uint16_t Dacbuffer[1024];





void wave_init(void);
void wave_fillbuffer(uint16_t* buffer, uint8_t type, uint16_t len);
void PlaybackData(uint8_t num);



#endif /* MY_USER_H_ */
