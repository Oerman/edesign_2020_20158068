/*
 * my_user.c
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////
///Code by Dr L Visagie Stellenbosch University//////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
#include "my_user.h"
#include <math.h>
#include <stdint.h>



int16_t sintab[256];
uint16_t offset_440hz;
uint16_t offset_523hz;


void wave_init()
{
	for (int i = 0; i < 256; i++)
		sintab[i] = (int) (1000.0f * sinf(i * 0.02454369261f));

	offset_440hz = 0;
	offset_523hz = 0;
}


void wave_fillbuffer(uint16_t* buffer, uint8_t type, uint16_t len)
{
	for (int i = 0; i < len; i++)
	{
		int16_t sample = 0;
		switch (type)
		{
		case 1:
			sample = sintab[offset_440hz >> 8];
			offset_440hz += 654;
			break;
		case 2:
			sample = sintab[offset_523hz >> 8];
			offset_523hz += 777;
			break;
		case 3:
			sample = (sintab[offset_440hz >> 8] >> 1) + (sintab[offset_523hz >> 8] >> 1);
			offset_440hz += 654;
			offset_523hz += 777;
			break;
		}
		buffer[i] = (uint16_t)(sample + 2048);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlaybackData(uint8_t num){

	wave_fillbuffer(Dacbuffer, num, 1024);
	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)Dacbuffer, 1024, DAC_ALIGN_12B_R);

}
//Uart message code
////////////some of the code was graciously provided by J Enright//////////////////////////////////////////////
////////////////////SU: 20987056////////////////////////////////////////////////////////////////////////////////

void uart_receive(void){
	HAL_UART_Receive_IT(&huart2, (uint8_t*)&dataRX, 1);
}


void uart_transmit(void){
	HAL_UART_Transmit(&huart2, (uint8_t*)com, 10, 100);

}

void comREC(void){

	com[0]=127;
	com[1]=128;
	com[2]='R';
	com[3]='e';
	com[4]='c';
	com[5]='o';
	com[6]='r';
	com[7]='d';
	com[8]='_';

	if(Rec ==1){
		com[9]='1';
	}
	if(Rec ==2){
		com[9]='2';
	}
	if(Rec ==3){
		com[9]='3';
	}


}
void comPlay(void){

	com[0]=127;
	com[1]=128;
	com[2]='P';
	com[3]='l';
	com[4]='a';
	com[5]='y';
	com[6]='_';
	com[7]='_';
	com[8]='_';

	if(Play ==1){
		com[9]='1';
	}
	if(Play ==2){
		com[9]='2';
	}
	if(Play ==3){
		com[9]='3';
	}


}
void comStop(void){

	com[0]=127;
	com[1]=128;
	com[2]='S';
	com[3]='t';
	com[4]='o';
	com[5]='p';
	com[6]='_';
	com[7]='_';
	com[8]='_';
	com[9]='_';




}

void RESETfunc(void){
	//but1 GPIOA, GPIO_PIN_9
	//but2 GPIOC, GPIO_PIN_7
	//but3 GPIOA, GPIO_PIN_10
	//rec GPIOB, GPIO_PIN_5
HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);

stopbutton = false;
recbutton = false;
button1 = false;
button2 = false;
button3 = false;
Rec=0;
Play=0;
toggle=0;


memset(Dacbuffer, 0, sizeof Dacbuffer);

}

void Stop(void){
	int state;
	  //stop button pressed
		    	if(buttonSTOP_pressed==1){
		    		//debouce the button for 10ms
		    		if((HAL_GetTick()-pretick) > 10){
		    			state =HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14);
		    			while(state==1){
		    				if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)==0)){


		    					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
		    					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
		    					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
		    					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);


		    						comStop();
		    						uart_transmit();
		    						RESETfunc();
		    			}
		    			}
		    		}

		    	}
}

void Record(void){

	//Record button pressed
	    	if((buttonREC_pressed==1)&&(button1_pressed==0)&&(button2_pressed==0)&&(button3_pressed==0)&&(buttonSTOP_pressed==0)){
	    		//debouce the button for 10ms
	    		if((HAL_GetTick()-pretick) > 10){

	    			while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_11)==1){

	    			//button 1 pressed
	    				if((button1_pressed==1)&&(button2_pressed==0)&&(button3_pressed==0)){
	    					int state;
	    				//debouce the button for 10ms
	    					if((HAL_GetTick()-pretick) > 10){
	    						state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13);
	    						while(state==1){

	    						if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)==0)){

	    							Rec = 1;

	    							comREC();
	    							uart_transmit();

	    							wave_fillbuffer(Dacbuffer, Rec, 1024);
	    							HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);

	    							if((flashflag)==1){
	    								flashflag=0;
	    								flashtick=0;
	    								HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);

	    						}
	    							RESETfunc();

	    						}

	    					}

	    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)==0){
	    							   	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
	    							  	 }

	    						pretick = HAL_GetTick();
	    				}
	    		}
	    				//BUTTON 2 pressed
	    				if((button1_pressed==0)&&(button2_pressed==1)&&(button3_pressed==0)){
	    					    					int state;
	    					    				//debouce the button for 10ms
	    					    					if((HAL_GetTick()-pretick) > 10){
	    					    						state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15);
	    					    						while(state==1){

	    					    						if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)==0)){

	    					    							Rec = 2;

	    					    							comREC();
	    					    							uart_transmit();

	    					    							wave_fillbuffer(Dacbuffer, Rec, 1024);
	    					    							HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);

	    					    							if((flashflag)==1){
	    					    								flashflag=0;
	    					    								flashtick=0;
	    					    								HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);

	    					    						}
	    					    						RESETfunc();
	    					    						}
	    					    					}

	    					    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)==0){
	    					    							   	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
	    					    							  	 }

	    					    						pretick = HAL_GetTick();
	    					    				}
	    					    		}
	    					//button 3 pressed
	    				if((button1_pressed==0)&&(button2_pressed==0)&&(button3_pressed==1)){
	    					    					int state;
	    					    				//debouce the button for 10ms
	    					    					if((HAL_GetTick()-pretick) > 10){
	    					    						state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12);
	    					    						while(state==1){

	    					    						if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)==0)){

	    					    							Rec = 3;

	    					    							comREC();
	    					    							uart_transmit();

	    					    							wave_fillbuffer(Dacbuffer, Rec, 1024);
	    					    							HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);

	    					    							if((flashflag)==1){
	    					    								flashflag=0;
	    					    								flashtick=0;
	    					    								HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_10);

	    					    						}
	    					    							RESETfunc();
	    					    						}
	    					    					}

	    					    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)==0){
	    					    							   	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
	    					    							  	 }

	    					    						pretick = HAL_GetTick();
	    					    				}
	    					    		}
	    	}
	    	}

	    		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 1);
	    	}

}



void Playfunc(void){
	//button 1 pressed
		    				if((button1_pressed==1)&&(button2_pressed==0)&&(button3_pressed==0)){
		    					int state;
		    				//debouce the button for 10ms
		    					if((HAL_GetTick()-pretick) > 10){
		    						state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13);
		    						while(state==1){

		    						if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)==0)){

		    							HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
		    							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
		    							HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1);
		   		    					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
		    							Rec = 1;

		    							comPlay();
		    							uart_transmit();

		    							wave_fillbuffer(Dacbuffer, Rec, 1024);
		    							HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);

		    							if((flashflag)==1){
		    								flashflag=0;
		    								flashtick=0;
		    								HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);

		    						}
		    							RESETfunc();

		    						}

		    					}

		    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)==0){
		    							   	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
		    							  	 }

		    						pretick = HAL_GetTick();
		    				}
		    		}
		    				//BUTTON 2 pressed
		    				if((button1_pressed==0)&&(button2_pressed==1)&&(button3_pressed==0)){
		    					    					int state;
		    					    				//debouce the button for 10ms
		    					    					if((HAL_GetTick()-pretick) > 10){
		    					    						state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15);
		    					    						while(state==1){

		    					    						if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)==0)){
		    					    							HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 1);
		    					    							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
		    					    							HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
		    					    							HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
		    					    							Rec = 2;

		    					    							comPlay();
		    					    							uart_transmit();

		    					    							wave_fillbuffer(Dacbuffer, Rec, 1024);
		    					    							HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);

		    					    							if((flashflag)==1){
		    					    								flashflag=0;
		    					    								flashtick=0;
		    					    								HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_7);

		    					    						}
		    					    						RESETfunc();
		    					    						}
		    					    					}

		    					    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)==0){
		    					    							   	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
		    					    							  	 }

		    					    						pretick = HAL_GetTick();
		    					    				}
		    					    		}
		    					//button 3 pressed
		    				if((button1_pressed==0)&&(button2_pressed==0)&&(button3_pressed==1)){
		    					    					int state;
		    					    				//debouce the button for 10ms
		    					    					if((HAL_GetTick()-pretick) > 10){
		    					    						state = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12);
		    					    						while(state==1){

		    					    						if((state==1)&&(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)==0)){
		    					    							HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, 0);
		    					    						    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, 0);
		    					    							HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
		    					  		    					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);
		    					    							Rec = 3;

		    					    							comPlay();
		    					    							uart_transmit();

		    					    							wave_fillbuffer(Dacbuffer, Rec, 1024);
		    					    							HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,(uint32_t*) Dacbuffer ,1024, DAC_ALIGN_12B_R);

		    					    							if((flashflag)==1){
		    					    								flashflag=0;
		    					    								flashtick=0;
		    					    								HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_10);

		    					    						}
		    					    							RESETfunc();
		    					    						}
		    					    					}

		    					    						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)==0){
		    					    							   	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
		    					    							  	 }

		    					    						pretick = HAL_GetTick();
		    					    				}
		    					    		}

}



