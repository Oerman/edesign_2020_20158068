/*
 * my_globals.c
 *
 */
#include <stdio.h>
#include <stdbool.h>

uint8_t buffer_tx[10] = {127,128,'2','0','1','5','8','0','6','8'};
uint8_t buffer_play1[10] = {127,128,'P','L','A','Y','_','_','_','1'};
uint8_t buffer_play2[10] = {127,128,'P','L','A','Y','_','_','_','2'};
uint8_t buffer_play3[10] = {127,128,'P','L','A','Y','_','_','_','3'};
uint8_t buffer_STOP[10] = {127,128,'S','T','O','P','_','_','_','_'};
uint16_t Dacbuffer[1024];

volatile uint8_t dataRX;
char com[10] = "0000000000";


int pretick;
int flashtick;

//button state variables
volatile uint8_t ButtonWithREC;
volatile int flashflag;
volatile uint8_t button1_pressed =0;
volatile uint8_t button2_pressed =0;
volatile uint8_t button3_pressed =0;
volatile uint8_t buttonREC_pressed =0;
volatile uint8_t buttonSTOP_pressed =0;
volatile uint8_t Sine =0;
volatile uint8_t Rec =0;
volatile uint8_t Play =0;
volatile uint8_t toggle=0;

//button state bool variables
volatile bool stopbutton = false;
volatile bool recbutton = false;
volatile bool button1 = false;
volatile bool button2 = false;
volatile bool button3 = false;

/////////////////////////////////////////////////////////////
//MICROPHONE variables

uint8_t recbuff[1024];
int32_t temp;
int8_t outputrec[1024];
int32_t avg =128;
int32_t accumalator;
int32_t numavg;
float smoothed_sample;

